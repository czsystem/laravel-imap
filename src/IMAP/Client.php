<?php

namespace CloudZentral\IMAP;

use CloudZentral\IMAP\Exceptions\ClientException;
use CloudZentral\IMAP\Support\FolderCollection;
use CloudZentral\IMAP\Support\MessageCollection;
use Ddeboer\Imap\Server;

/**
 * Class Client
 * @package CloudZentral\IMAP
 */
class Client
{
    private static $instance;

    private $settings = [
        'host' => "",
        'port' => 993,
        'username' => "",
        'password' => ""
    ];

    private $client;
    private $connection;

    /**
     * Get instance.
     * @param array|null $settings
     * @param bool $reset
     * @return Client
     * @throws ClientException
     */
    public static function getInstance(?array $settings = null, bool $reset = false)
    {
        if(is_null(self::$instance) || $reset) {
            if(is_null($settings) || empty($settings)) {
                throw new ClientException("Settings can not be null or empty when no instance exists");
            }
            return self::$instance = new self($settings);
        } else {
            return self::$instance;
        }
    }

    /**
     * Client constructor.
     * @param array $settings
     */
    private function __construct(array $settings)
    {
        $this->settings = array_merge($this->settings, $settings);

        $this->client = new Server(
            $this->settings['host'],
            $this->settings['port']
        );

        $this->connection = $this->client->authenticate(
            $this->settings['username'],
            $this->settings['password']
        );
    }

    /**
     * Get folders.
     * @param string|null $fullName
     * @param bool $preloadChildren
     * @return FolderCollection
     */
    public function getFolders(string $fullName = null, bool $preloadChildren = false): FolderCollection
    {
        $folders = [];

        $mailboxes = $this->connection->getMailboxes();
        foreach ($mailboxes as $mailbox) {
            $name = $mailbox->getName();

            // Skip container-only mailboxes
            // @see https://secure.php.net/manual/en/function.imap-getmailboxes.php
            if ($mailbox->getAttributes() & \LATT_NOSELECT) {
                continue;
            }

            if(is_null($fullName)) {
                if(substr_count($name, ".") === 0) {
                    $folders[] = new Folder($this, $mailbox, $preloadChildren);
                }
            } else {
                if(substr_count($name, ".") > 0) {
                    $cut = substr($name, 0, strrpos($name, "."));
                    if($cut === $fullName) {
                        $folders[] = new Folder($this, $mailbox, $preloadChildren);
                    }
                }
            }
        }

        return FolderCollection::make($folders);
    }

    /**
     * Get a folder.
     * @param string $fullName
     * @param bool $preloadChildren
     * @return Folder
     */
    public function getFolder(string $fullName, bool $preloadChildren = false): Folder
    {
        $mailbox = $this->connection->getMailbox($fullName);
        return new Folder($this, $mailbox, $preloadChildren);
    }

    /**
     * Get messages.
     * @param string $folder_fullName
     * @return MessageCollection
     */
    public function getMessages(string $folder_fullName): MessageCollection
    {
        $mailbox = $this->connection->getMailbox($folder_fullName);
        $messages = $mailbox->getMessages();

        $arr = [];
        foreach($messages as $message) {
            $arr[] = new DdeboerMessage($message);
        }

        return MessageCollection::make($arr)->sortByDesc('timestamp');
    }

    /**
     * Get a message.
     * @param string $folder_fullName
     * @param int $number
     * @return DdeboerMessage
     */
    public function getMessage(string $folder_fullName, int $number): DdeboerMessage
    {
        $mailbox = $this->connection->getMailbox($folder_fullName);
        $message = $mailbox->getMessage($number);
        return new DdeboerMessage($message);
    }

    /**
     * Add message.
     * @param string $folder_fullName
     * @param DraftMessage $draftMessage
     * @return bool
     */
    public function addMessage(string $folder_fullName, DraftMessage $draftMessage): bool
    {
        $mailbox = $this->connection->getMailbox($folder_fullName);
        return $mailbox->addMessage($draftMessage->getMimeString(), $draftMessage->flags);
    }
}
