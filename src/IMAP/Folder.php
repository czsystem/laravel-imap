<?php

namespace CloudZentral\IMAP;

use CloudZentral\IMAP\Support\FolderCollection;
use CloudZentral\IMAP\Support\MessageCollection;
use Ddeboer\Imap\Mailbox;

/**
 * Class Folder
 * @package CloudZentral\IMAP
 */
class Folder
{
    /**
     * @var FolderCollection
     */
    private $children;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var Mailbox
     */
    public $mailbox;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $fullName;

    /**
     * @var string
     */
    public $encodedName;

    /**
     * @var string
     */
    public $fullEncodedName;

    /**
     * Folder constructor.
     * @param Client $client
     * @param Mailbox $mailbox
     * @param bool $preloadChildren
     */
    public function __construct(Client $client, Mailbox $mailbox, bool $preloadChildren = false)
    {
        $this->client = $client;
        $this->mailbox = $mailbox;

        $this->fullName = $mailbox->getName();
        $this->encodedName = $mailbox->getEncodedName();
        $this->fullEncodedName = $mailbox->getFullEncodedName();
        $this->name = $this->makeName();

        if($preloadChildren) {
            $this->refreshChildren($preloadChildren);
        }
    }

    /**
     * __get accessor.
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if($name === "children") {
            if(is_null($this->children)) {
                $this->refreshChildren();
            }
            return $this->children;
        }
    }

    /**
     * Make name from full name.
     * @return string
     */
    private function makeName()
    {
        $fullName = $this->fullName;
        if(strpos($fullName, ".") !== false) {
            $last_pos = strrpos($fullName, ".");
            $start_pos = ($last_pos + 1);
            $name = substr($fullName, $start_pos);
            return trim($name);
        } else {
            return $fullName;
        }
    }

    /**
     * Refresh children collection.
     * @param bool $preloadChildren
     */
    public function refreshChildren(bool $preloadChildren = false)
    {
        $this->children = $this->client->getFolders($this->fullName, $preloadChildren);
    }

    /**
     * Check if has children.
     * @return bool
     */
    public function hasChildren(): bool
    {
        $this->refreshChildren();
        return !$this->children->isEmpty();
    }

    /**
     * Get messages.
     * @return MessageCollection
     */
    public function getMessages(): MessageCollection
    {
        return $this->client->getMessages($this->fullName);
    }

    /**
     * Get a message.
     * @param int $number
     * @return DdeboerMessage
     */
    public function getMessage(int $number): DdeboerMessage
    {
        return $this->client->getMessage($this->fullName, $number);
    }

    /**
     * Add a message.
     * @param DraftMessage $draftMessage
     * @return bool
     */
    public function addMessage(DraftMessage $draftMessage): bool
    {
        $this->client->addMessage($this->fullName, $draftMessage);
    }
}
