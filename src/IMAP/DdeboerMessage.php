<?php

namespace CloudZentral\IMAP;

use CloudZentral\IMAP\Support\EmailAddressCollection;
use Ddeboer\Imap\MessageInterface;

/**
 * Class DdeboerMessage
 * @package CloudZentral\IMAP
 */
class DdeboerMessage extends Message
{
    /**
     * @var Message
     */
    private $message;

    /**
     * @var int
     */
    private $number;

    /**
     * DdeboerMessage constructor.
     * @param Message $message
     */
    public function __construct(MessageInterface $message)
    {
        $this->message = $message;

        $from_name = is_null($message->getFrom()) ? null : $message->getFrom()->getName();
        $from_address = is_null($message->getFrom()) ? null : $message->getFrom()->getAddress();

        $from = new EmailAddress($from_name, $from_address);
        $to = EmailAddressCollection::make([]);
        foreach ($message->getTo() as $emailAddress) {
            $to->push(new EmailAddress($emailAddress->getName(), $emailAddress->getAddress()));
        }

        $flags = [];
        if($message->isSeen()) {
            $flags[] = Message::FLAG_SEEN;
        }

        $date = is_null($message->getDate()) ? null : $message->getDate()->format("Y-m-d H:i:s");

        parent::__construct(
            $message->getSubject(),
            $date,
            $from,
            $to,
            null,
            null,
            implode(" ", $flags)
        );

        $this->number = $message->getNumber();
    }

    /**
     * __get.
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if($name === "bodyHtml") {
            $this->bodyHtml = $this->message->getBodyHtml();
        }

        if($name === "bodyText") {
            $this->bodyText = $this->message->getBodyText();
        }

        return $this->{$name};
    }
}
