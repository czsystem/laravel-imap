<?php

namespace CloudZentral\IMAP;

use Carbon\Carbon;

/**
 * Example of mime found here: https://www.php.net/manual/en/function.imap-append.php
 */

/**
 * Class MessageDraft
 * @package CloudZentral\IMAP
 */
class MessageDraftOld
{
    const FLAG_SEEN = '\\Seen';

    public $from;
    public $to;
    public $subject;
    public $body;
    public $flags;

    /**
     * MessageDraft constructor.
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param array $flags
     */
    public function __construct(string $from, string $to, string $subject, string $body, array $flags = [])
    {
        $this->from = $from;
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
        $this->flags = $flags;
    }

    /**
     * Get mime string.
     * @return string
     */
    public function getMimeString(): string
    {
        $boundary1 = "###".md5(microtime())."###";
        $boundary2 = "###".md5(microtime().rand(99,999))."###";
        $date = Carbon::now()->format("Y-m-d H:i:s");

        $string = "";

        $string .= "From: $this->from\r\n";
        $string .= "To: $this->to\r\n";
        $string .= "Date: $date\r\n";
        $string .= "Subject: $this->subject\r\n";

        $string .= "MIME-version: 1.0\r\n";
        $string .= "Content-Type: multipart/mixed; boundary=\"$boundary1\"\r\n";
        $string .= "\r\n\r\n";
        $string .= "--$boundary1\r\n";
        $string .= "Content-Type: multipart/alternative; boundary=\"$boundary2\"\r\n";
        $string .= "\r\n\r\n";

        // ADD Plain text data
        $string .= "--$boundary2\r\n";
        $string .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
        $string .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $string .= "\r\n\r\n";
        $string .= strip_tags($this->body)."\r\n";
        $string .= "\r\n\r\n";

        // ADD html data
        $string .= "--$boundary2\r\n";
        $string .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        $string .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $string .= "\r\n\r\n";
        $string .= html_entity_decode($this->body)."\r\n";
        $string .= "\r\n\r\n";

        return $string;
    }

    /**
     * Get flags string.
     * @return string
     */
    public function getFlagsString(): string
    {
        return implode(" ", $this->flags);
    }
}
