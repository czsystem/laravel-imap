<?php

namespace CloudZentral\IMAP;

use Carbon\Carbon;
use CloudZentral\IMAP\Support\EmailAddressCollection;

/**
 * Class Message
 * @package CloudZentral\IMAP
 */
class MessageOld
{
    private $client;
    private $message;

    public $id;
    public $number;
    public $subject;
    public $date;
    public $from;
    public $to;
    public $isAnswered;
    public $isDeleted;
    public $isDraft;
    public $isSeen;
    private $bodyHtml;
    private $bodyText;

    /**
     * Message constructor.
     * @param Client $client
     * @param \Ddeboer\Imap\Message $message
     */
    public function __construct(Client $client, \Ddeboer\Imap\Message $message)
    {
        $this->client = $client;
        $this->message = $message;

        $this->id = $message->getId();
        $this->number = $message->getNumber();
        $this->subject = $message->getSubject();
        $this->date = Carbon::createFromFormat("Y-m-d H:i:s", $message->getDate()->format("Y-m-d H:i:s"));
        $this->from = new EmailAddress($message->getFrom());

        $to = [];
        foreach ($message->getTo() as $emailAddress) {
            $to[] = new EmailAddress($emailAddress);
        }
        $this->to = EmailAddressCollection::make($to);

        $this->isAnswered = $message->isAnswered();
        $this->isDeleted = $message->isDeleted();
        $this->isDraft = $message->isDraft();
        $this->isSeen = $message->isSeen();
    }

    /**
     * __get accessor.
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if($name === "bodyHtml") {
            if(is_null($this->bodyHtml)) {
                $this->refreshBodyHtml();
            }
            return $this->bodyHtml;
        }

        if($name === "bodyText") {
            if(is_null($this->bodyText)) {
                $this->refreshBodyText();
            }
            return $this->bodyText;
        }
    }

    /**
     * Refresh body html.
     */
    private function refreshBodyHtml()
    {
        $this->bodyHtml = $this->message->getBodyHtml();
    }

    /**
     * Refresh body text.
     */
    private function refreshBodyText()
    {
        $this->bodyText = $this->message->getBodyText();
    }
}
