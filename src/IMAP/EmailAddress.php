<?php

namespace CloudZentral\IMAP;

/**
 * Class EmailAddress
 * @package CloudZentral\IMAP
 */
class EmailAddress
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $address;


    /**
     * EmailAddress constructor.
     * @param string|null $name
     * @param string|null $address
     */
    public function __construct(?string $name, ?string $address)
    {
        $this->name = $name;
        $this->address = $address;
    }

    /**
     * __toString.
     * @return string|null
     */
    public function __toString()
    {
        if(is_null($this->name)) {
            return $this->address;
        } else {
            return $this->name.' <'.$this->address.'>';
        }
    }
}
