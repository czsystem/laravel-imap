<?php

namespace CloudZentral\IMAP\Providers;

/**
 * Class ServiceProvider
 * @package CloudZentral\IMAP\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
