<?php

namespace CloudZentral\IMAP;

use Carbon\Carbon;
use CloudZentral\IMAP\Support\EmailAddressCollection;

/**
 * Class DraftMessage
 * @package CloudZentral\IMAP
 */
class DraftMessage extends Message
{
    /**
     * DraftMessage constructor.
     * @param string $subject
     * @param EmailAddress $from
     * @param EmailAddressCollection $to
     * @param string $bodyHtml
     * @param string $bodyText
     * @param array $flags
     */
    public function __construct(string $subject, EmailAddress $from, EmailAddressCollection $to, string $bodyHtml, string $bodyText, array $flags = [])
    {
        $flags = implode(" ", $flags);
        $date = Carbon::now()->format("Y-m-d H:i:s");
        parent::__construct($subject, $date, $from, $to, $bodyHtml, $bodyText, $flags);
    }

    /**
     * Get mime string.
     * @return string
     */
    public function getMimeString(): string
    {
        $boundary1 = "###".md5(microtime())."###";
        $boundary2 = "###".md5(microtime().rand(99,999))."###";

        $string = "";

        $string .= "From: $this->from\r\n";
        $string .= "To: $this->to\r\n";
        $string .= "Date: $this->date\r\n";
        $string .= "Subject: $this->subject\r\n";

        $string .= "MIME-version: 1.0\r\n";
        $string .= "Content-Type: multipart/mixed; boundary=\"$boundary1\"\r\n";
        $string .= "\r\n\r\n";
        $string .= "--$boundary1\r\n";
        $string .= "Content-Type: multipart/alternative; boundary=\"$boundary2\"\r\n";
        $string .= "\r\n\r\n";

        // ADD Plain text data
        $string .= "--$boundary2\r\n";
        $string .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
        $string .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $string .= "\r\n\r\n";
        $string .= strip_tags($this->bodyText)."\r\n";
        $string .= "\r\n\r\n";

        // ADD html data
        $string .= "--$boundary2\r\n";
        $string .= "Content-Type: text/html; charset=\"utf-8\"\r\n";
        $string .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $string .= "\r\n\r\n";
        $string .= html_entity_decode($this->bodyHtml)."\r\n";
        $string .= "\r\n\r\n";

        return $string;
    }
}
