<?php

namespace CloudZentral\IMAP\Support;

/**
 * Class EmailAddressCollection
 * @package CloudZentral\IMAP\Support
 */
class EmailAddressCollection extends Collection
{
    /**
     * __toString.
     * @return string
     */
    public function __toString()
    {
        return implode(";", $this->toArray());
    }
}
