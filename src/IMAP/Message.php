<?php

namespace CloudZentral\IMAP;

use Carbon\Carbon;
use CloudZentral\IMAP\Support\EmailAddressCollection;

/**
 * Class Message
 * @package CloudZentral\IMAP
 */
class Message
{
    const FLAG_SEEN = '\\Seen';

    /**
     * @var string|null
     */
    public $subject;

    /**
     * @var Carbon|null
     */
    public $date;

    /**
     * @var int
     */
    public $timestamp;

    /**
     * @var EmailAddress|null
     */
    public $from;

    /**
     * @var EmailAddressCollection|null
     */
    public $to;

    /**
     * @var string|null
     */
    protected $bodyHtml;

    /**
     * @var string|null
     */
    protected $bodyText;

    /**
     * @var string|null
     */
    public $flags;

    /**
     * @var bool
     */
    public $isSeen;

    /**
     * Message constructor.
     * @param string|null $subject
     * @param string|null $date
     * @param EmailAddress|null $from
     * @param EmailAddressCollection|null $to
     * @param string|null $bodyHtml
     * @param string|null $bodyText
     * @param string|null $flags
     */
    public function __construct(?string $subject, ?string $date, ?EmailAddress $from, ?EmailAddressCollection $to, ?string $bodyHtml, ?string $bodyText, ?string $flags)
    {
        $this->subject = $subject;
        $this->date = is_null($date) ? null : Carbon::createFromFormat("Y-m-d H:i:s", $date);
        if(!is_null($this->date)) {
            $this->timestamp = $this->date->getTimestamp();
        }
        $this->from = $from;
        $this->to = $to;
        $this->bodyHtml = $bodyHtml;
        $this->bodyText = $bodyText;
        $this->flags = $flags;
        $this->processFlags();
    }

    /**
     * __get.
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->{$name};
    }

    /**
     * Process flags.
     */
    private function processFlags()
    {
        $flags = !is_null($this->flags) ? explode(" ", $this->flags) : [];

        $this->isSeen = in_array(self::FLAG_SEEN, $flags);
    }
}
